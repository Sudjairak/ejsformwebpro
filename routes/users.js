var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/result', function(req, res, next) {
  res.render('result.ejs', {
    user: req.body.user,
    pass: req.body.pass
  });
});

module.exports = router;
