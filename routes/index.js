var express = require('express');
var router = express.Router();
var app = express();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('register.ejs', { title: 'Express' });
});

router.post('/result', function(req, res, next) {
  res.render('result.ejs', {
    id: req.body.id,
    prefix: req.body.prefix,
    fname: req.body.fname,
    lname: req.body.lname,
    nickname: req.body.nickname,
    birthdate: req.body.birthdate,
    province: req.body.province,
    county: req.body.county,
    district: req.body.district,
    houseno: req.body.houseno,
    road: req.body.road,
    zips: req.body.zips,
    telephone: req.body.telephone,
    fax: req.body.fax,
    smartphone: req.body.smartphone,
    email: req.body.email,
    prefix_en: req.body.prefix_en,
    fname_en: req.body.fname_en,
    lname_en: req.body.lname_en
  });
});
app.use(express.static(__dirname+'/public'));

module.exports = router;
